<?php

namespace Drupal\search_api_sirsi\Form;


use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal;
use Drupal\search_api\Entity\Index;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\SearchApiException;


/**
 * @see \Drupal\Core\Form\FormBase
 */
class search_api_sirsiSearchForm extends FormBase {

  use MessengerTrait;

  /**
   * @return string[]
   */
  protected function getEditableConfigNames() {
    return [
      'search_api_sirsi.formsettings',
    ];
  }

  /**
   * @return string
   *   Form ID
   */
  public function getFormId() {
    return 'search_api_sirsi_searchform';
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
    $default_value = '';
    if (isset($args['keys'])) {
      $default_value = $args['keys'];
    }
    if ($default_value === '') {
      $default_value = $this->getRequest()->get('keys');
    }
    $form['#tree'] = TRUE;
    $form['contain_div_start'] = [
      '#markup' => '<div id="search-api-sirsi-search-inner">',
    ];

    $form['row_1'] = [
      '#markup' => '<div id="search-api-sirsi-search-row">',
    ];

    $form['keys'] = [
      '#type' => 'search',
      '#title' => 'Search',
      '#title_display' => 'invisible',
      '#size' => 15,
      '#default_value' => $default_value,
      '#attributes' => ['title' => 'Enter the terms you wish to search for.'],
    ];

    $form['row_1_end'] = [
      '#markup' => '</div>',
    ];

    $form['filters_fieldset']['actions'] = [
      '#type' => 'actions',
    ];

    $form['filters_fieldset']['actions']['spacer'] = [
      '#prefix' => '<div id="space-div-horiz">',
      '#suffix' => '</div>',
    ];

    $form['actions']['button_row_2'] = [
      '#markup' => '<div id="search-api-sirsi-search-row">',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
      '#id' => 'msi-src-button-submit',
    ];

    $form['actions']['reset'] = [
      '#type' => 'button',
      '#value' => $this->t('Clear'),
      '#prefix' => '<div id="search-api-sirsi-clr-btn">',
      '#suffix' => '</div>',
      '#attributes' => ['onclick' => 'return false;'],
    ];

    $form['actions']['button_row_2_end'] = [
      '#markup' => '</div>',
    ];

    $form['contain_div_end'] = [
      '#markup' => '</div>',
    ];

    $results = $form_state->get('results');
    $renderer = \Drupal::service('renderer');
    $form['results'] = [
      '#prefix' => '<div id="results">',
      '#suffix' => '</div>',
      '#markup' => $renderer->render($results),
    ];
    /* Match the search form styling of Drupal core. */
    $form['#attributes']['class'][] = 'search-form';
    $form['#attributes']['class'][] = 'search-block-form';
    $form['#attributes']['class'][] = 'container-inline';
    $form['actions']['submit']['#attributes']['class'][] = 'search-form__submit';
    return $form;
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * has to exist
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    #
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $indexName = Drupal::config('search_api_sirsi.adminsettings')->get('index_name');
    $keys = $form_state->getValue('keys');
    /* @var $index \Drupal\search_api\IndexInterface */
    $index = Index::load($indexName);
    $query = $index->query();
    $query->keys($keys);
    $query->setFulltextFields(['body']);
    /* parse mode */
    $parse_mode = Drupal::service('plugin.manager.search_api.parse_mode')
      ->createInstance('terms');
    $query->setParseMode($parse_mode);
    $response = $query->execute();
    $results = $response->getResultItems();
    $start = 0;
    $end = 10;
    foreach($results as $result) {
      $resultArray[] = $this->createFormItemRenderArray($result);
    }
    $form_state->setRebuild();
    $finalArray = array_slice($resultArray, $start, $end);
    /* output */
    $form_state->set('results', $finalArray);
  }

  /**
   * Creates a render array for the given result item.
   *
   * @param \Drupal\search_api\Item\ItemInterface $item
   *   The item to render.
   *
   * @return array
   *   A render array for the given result item.
   */
  protected function createFormItemRenderArray(ItemInterface $item) {
    try {
      $originalObject = $item->getOriginalObject();
      if ($originalObject === NULL) {
        return [];
      }
      /** @var \Drupal\Core\Entity\EntityInterface $entity */
      $entity = $originalObject->getValue();
    }
    catch (SearchApiException $e) {
      return [];
    }
    if (!$entity) {
      return [];
    }
    $viewedResult = [
      '#theme' => 'search_api_sirsi_search_result',
      '#item' => $item,
      '#entity' => $entity,
    ];
    return $viewedResult;
  }


}
