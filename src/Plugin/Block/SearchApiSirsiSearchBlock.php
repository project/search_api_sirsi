<?php

namespace Drupal\search_api_sirsi\Plugin\Block;

use Drupal;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a Search Block.
 *
 * @Block(
 *   id = "search_api_sirsi_search_block",
 *   admin_label = @Translation("Search API Sirsi Search Block"),
 * )
 */
class SearchApiSirsiSearchBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $build = array(
      '#theme' => 'search_api_sirsi_search_block',
      '#attached' => [
        'library' => [
          'search_api_sirsi/search_api_sirsi.library',
        ],
      ],
      '#search_block_form' => $this->buildSearchApiSirsiSearchBlock($config),
    );
    $build['#cache']['max-age'] = 0;
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['search_api_sirsi_search'] = array(
      '#type' => 'markup',
      '#markup' => 'There is no configuration for this block.',
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    #
  }

  /**
   * Build the search_api_sirsi search block
   * @param  array $config  array of block configs
   * @return array form render array
   */
  public function buildSearchApiSirsiSearchBlock($config) {
    $searchForm = Drupal::formBuilder()->getForm('Drupal\search_api_sirsi\Form\search_api_sirsiPageForm');
    $rendered[] = [$searchForm];
    return $rendered;
  }
}
