<?php

namespace Drupal\search_api_sirsi\Entity;


use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Link;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal;


class Sirsi implements EntityInterface {

  /**
   * @var array
   */
  protected $values = [];

  /**
   * @var string
   */
  public $entityTypeId;

  /**
   * @var int|string|null
   */
  public $entityId;

  /**
   * @var mixed
   */
  public $author;

  /**
   * @var mixed
   */
  public $title;

  /**
   * @var mixed
   */
  public $language;

  /**
   * @var mixed
   */
  public $summary;

  /**
   * @var mixed
   */
  public $bibsummary;

  /**
   * @var mixed
   */
  public $initial_title_srch;

  /**
   * @var mixed
   */
  public $pubdate;

  /**
   * @var mixed
   */
  public $publisher;

  /**
   * Sirsi constructor.
   * @param null $data
   * @TODO: handle langcodes / languages
   */
  public function __construct($data = NULL) {
    $id = key($data);
    $this->values = [
      'id' => [
        '#value' => $id
      ],
      'uuid' => [
        '#value' => $this->uuidGenerator()->generate()
      ],
      'author' => [
        '#value' => $data[$id]['author']
      ],
      'title' => [
        '#value' => $data[$id]['title']
      ],
      'type' => [
        '#value' => 'sirsi'
      ],
      'langcode' => [
        '#value' => 'en'
      ],
      'summary' => [
        '#value' => $data[$id]['summary']
      ],
      'bibsummary' => [
        '#value' => $data[$id]['bibsummary']
      ],

    ];
    $this->entityTypeId = 'sirsi';
    $this->entityId = $id;
    $this->author = $data[$id]['author'];
    $this->title = $data[$id]['title'];
    $this->language = $data[$id]['language'];
    $this->summary = $data[$id]['summary'];
    $this->bibsummary = $data[$id]['bibsummary'];
    $this->initial_title_srch = $data[$id]['initial_title_srch'];
    $this->pubdate = $data[$id]['pubdate'];
    $this->publisher = $data[$id]['publisher'];
  }

  /**
   * @param string $operation
   * @param AccountInterface|null $account
   * @param false $return_as_object
   * @return bool|\Drupal\Core\Access\AccessResultInterface|void
   * @TODO: Implement access() method.
   */
  public function access($operation, AccountInterface $account = NULL, $return_as_object = FALSE)
  {
    #
  }

  /**
   * @return string[]|void
   * @TODO: Implement getCacheContexts() method.
   */
  public function getCacheContexts()
  {
    #
  }

  /**
   * @return string[]|void
   * @TODO: Implement getCacheTags() method.
   */
  public function getCacheTags()
  {
    #
  }

  /**
   * @return int|void
   * @TODO: Implement getCacheMaxAge() method.
   */
  public function getCacheMaxAge()
  {
    #
  }

  /**
   * @return string|void|null
   * @TODO: Implement uuid() method.
   */
  public function uuid() {
    #
  }

  /**
   * @return int|string|null
   */
  public function id() {
    return $this->entityId;
  }

  /**
   * @return \Drupal\Core\Language\LanguageInterface|void
   * @TODO: Implement language() method.
   */
  public function language()
  {
    #
  }

  /**
   * @return bool|void
   * @TODO: Implement isNew() method.
   */
  public function isNew() {
    #
  }

  /**
   * @param bool $value
   * @return Sirsi|void
   * @TODO: Implement enforceIsNew() method.
   */
  public function enforceIsNew($value = TRUE)
  {
    #
  }

  /**
   * @return string
   *
   */
  public function getEntityTypeId() {
    return 'sirsi';
  }

  /**
   * @return string
   *
   */
  public function bundle() {
    return 'sirsi';
  }

  /**
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|mixed|string|null
   *
   */
  public function label() {
    return $this->title;
  }

  /**
   * @param string $rel
   * @param array $options
   * @return Url
   * use this when /catalog/ is constructed
   */
  public function toUrl($rel = 'canonical', array $options = []) {
    return Url::fromUserInput('/catalog/item/'.$this->entityId);
  }

  /**
   * @param null $text
   * @param string $rel
   * @param array $options
   * @return Link
   */
  public function toLink($text = NULL, $rel = 'canonical', array $options = []) {
    return Link::fromTextAndUrl($this->values['title'], Url::fromUserInput('/catalog/item/'.$this->entityId));
  }

  /**
   * @return mixed
   *
   */
  public function getTitle() {
      return $this->title;
  }

  /**
   * @return mixed
   *
   */
  public function uuidGenerator() {
    return Drupal::service('uuid');
  }

  /**
   * @param string $key
   * @return bool|void
   * @TODO: Implement hasLinkTemplate() method.
   */
  public function hasLinkTemplate($key)
  {
    #
  }

  /**
   * @return string[]|void
   * @TODO: Implement uriRelationships() method.
   */
  public function uriRelationships()
  {
    #
  }

  /**
   * @param mixed $id
   * @return Sirsi|void|null
   * @TODO: Implement load() method.
   */
  public static function load($id) {
    #
  }

  /**
   * @param array|null $ids
   * @return Sirsi[]|void
   * @TODO: Implement loadMultiple() method.
   */
  public static function loadMultiple(array $ids = NULL)
  {
    #
  }

  /**
   * @param array $values
   * @return Sirsi|void
   * @TODO: Impliment create() method.
   */
  public static function create(array $values = []) {
    #
  }

  /**
   * @return int
   *
   */
  public function save() {
    return 1;
  }

  /**
   * @TODO: Implement delete() method.
   */
  public function delete()
  {
    #
  }

  /**
   * @param EntityStorageInterface $storage
   * @TODO: Implement preSave() method.
   */
  public function preSave(EntityStorageInterface $storage)
  {
    #
  }

  /**
   * @param EntityStorageInterface $storage
   * @param bool $update
   * @TODO: Implement postSave() method.
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE)
  {
    #
  }

  /**
   * @param EntityStorageInterface $storage
   * @param array $values
   * @TODO: Implement preCreate() method.
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values)
  {
    #
  }

  /**
   * @param EntityStorageInterface $storage
   * @TODO: Implement postCreate() method.
   */
  public function postCreate(EntityStorageInterface $storage)
  {
    #
  }

  /**
   * @param EntityStorageInterface $storage
   * @param array $entities
   * @TODO: Implement preDelete() method.
   */
  public static function preDelete(EntityStorageInterface $storage, array $entities)
  {
    #
  }

  /**
   * @param EntityStorageInterface $storage
   * @param array $entities
   * @TODO: Implement postDelete() method.
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities)
  {
    #
  }

  /**
   * @param EntityStorageInterface $storage
   * @param array $entities
   * @TODO: Implement postLoad() method.
   */
  public static function postLoad(EntityStorageInterface $storage, array &$entities)
  {
    #
  }

  /**
   * @return Sirsi|void
   * @TODO: Implement createDuplicate() method.
   */
  public function createDuplicate()
  {
    #
  }

  /**
   * @return \Drupal\Core\Entity\EntityTypeInterface|void
   * @TODO: Implement getEntityType() method.
   */
  public function getEntityType() {
    #
  }

  /**
   * @return EntityInterface[]|void
   * @TODO: Implement referencedEntities() method.
   */
  public function referencedEntities()
  {
    #
  }

  /**
   * @return int|string|void|null
   * @TODO: Implement getOriginalId() method.
   */
  public function getOriginalId()
  {
    #
  }

  /**
   * @return string[]|void
   * @TODO: Implement getCacheTagsToInvalidate() method.
   */
  public function getCacheTagsToInvalidate()
  {
    #
  }

  /**
   * @param int|string|null $id
   * @return Sirsi|void
   * @TODO: Implement setOriginalId() method.
   */
  public function setOriginalId($id)
  {
    #
  }

  /**
   * @return array|mixed[]
   *
   */
  public function toArray() {
    return $this->values;
  }

  /**
   * @return \Drupal\Core\TypedData\ComplexDataInterface|void
   * @TODO: Implement getTypedData() method.
   */
  public function getTypedData()
  {
    #
  }

  /**
   * @return string|void
   * @TODO: Implement getConfigDependencyKey() method.
   */
  public function getConfigDependencyKey()
  {
    #
  }

  /**
   * @return string|void
   * @TODO: Implement getConfigDependencyName() method.
   */
  public function getConfigDependencyName()
  {
    #
  }

  /**
   * @return string|void
   * @TODO: Implement getConfigTarget() method.
   */
  public function getConfigTarget()
  {
    #
  }

  /**
   * @param array $cache_contexts
   * @return Sirsi|void
   * @TODO: Implement addCacheContexts() method.
   */
  public function addCacheContexts(array $cache_contexts)
  {
    #
  }

  /**
   * @param array $cache_tags
   * @return Sirsi|void
   * @TODO: Implement addCacheTags() method.
   */
  public function addCacheTags(array $cache_tags)
  {
    #
  }

  /**
   * @param int $max_age
   * @return Sirsi|void
   * @TODO: Implement mergeCacheMaxAge() method.
   */
  public function mergeCacheMaxAge($max_age)
  {
    #
  }

  /**
   * @param \Drupal\Core\Cache\CacheableDependencyInterface|object $other_object
   * @return Sirsi|void
   * @TODO: Implement addCacheableDependency() method.
   */
  public function addCacheableDependency($other_object)
  {
    #
  }

}
