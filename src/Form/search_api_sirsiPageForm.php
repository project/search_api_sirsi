<?php

namespace Drupal\search_api_sirsi\Form;


use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Url;


/**
 * Implements the SearchApiSirsiSearchForm form controller.
 * @see \Drupal\Core\Form\FormBase
 */
class search_api_sirsiPageForm extends FormBase {

  use MessengerTrait;

  /**
   * @return string[]
   */
  protected function getEditableConfigNames() {
    return [
      'search_api_sirsi_page.formsettings',
    ];
  }

  /**
   * @return string
   *   Form ID
   */
  public function getFormId() {
    return 'search_api_sirsi_page_searchform';
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state, $args = []) {
    $default_value = '';
    if (isset($args['keys'])) {
      $default_value = $args['keys'];
    }
    if ($default_value === '') {
      $request = $this->getRequest();
      $default_value = $this->getRequest()->get('keys');
    }
    if (trim($request->getPathInfo(), '/') == 'catalog') {
      $default_value = $this->getRequest()->get('keys');
    }

    $form['keys'] = [
      '#type' => 'search',
      '#title' => 'Search',
      '#title_display' => 'invisible',
      '#size' => 15,
      '#default_value' => $default_value,
      '#attributes' => ['title' => 'Enter the terms you wish to search for.'],
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
      '#id' => 'search-api-sirsi-src-button-submit',
    ];

    // Match the search form styling of Drupal core.
    $form['#attributes']['class'][] = 'search-form';
    $form['#attributes']['class'][] = 'search-block-form';
    $form['#attributes']['class'][] = 'container-inline';
    $form['actions']['submit']['#attributes']['class'][] = 'search-form__submit';
    return $form;
  }


  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * performs a search query based on form input
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    #
  }


  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $route = 'search_api_sirsi.searchpage';
    $routeArguments = [];
    if($form_state->getValue('keys') != NULL) {
      $routeArguments = ['keys' => $form_state->getValue('keys')];
    }
    $form_state->setRedirectUrl(Url::fromRoute($route, $routeArguments));


  }






}
