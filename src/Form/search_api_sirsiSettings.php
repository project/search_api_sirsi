<?php
/**
 * @file
 * Contains \Drupal\search_api_sirsi\Form\search_api_sirsiSettings.
 */
namespace Drupal\search_api_sirsi\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;


class search_api_sirsiSettings extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SIRSI_SEARCH_SETTINGS = 'search_api_sirsi.adminsettings';

  /**
   * @return string
   */
  public function getFormId() {
    return 'search_api_sirsi_admin_form';
  }

  /**
   * @return string[]
   *
   */
  protected function getEditableConfigNames() {
    return [
      static::SIRSI_SEARCH_SETTINGS,
    ];
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   *
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
		$config = $this->config(static::SIRSI_SEARCH_SETTINGS);

    $form['search'] = [
      '#type' => 'fieldset',
      '#title' => t('Search Form Settings'),
      '#open' => TRUE,
    ];

    $form['search']['discovery_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Discovery Search URL'),
      '#default_value' => $config->get('discovery_url'),
      '#required' => FALSE,
      '#description' => 'Discovery Search URL with no ending slash ex: "https://somesite.sirsi.net/discovery-search/os".',
    ];

    $form['search']['discovery_profile'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Discovery Search profile name'),
      '#default_value' => $config->get('discovery_profile'),
      '#required' => FALSE,
      '#description' => 'Discovery Search profile name, ex: "PROFILE1".',
    ];

    $form['search']['discovery_limit'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search result count limit.'),
      '#default_value' => $config->get('discovery_limit'),
      '#required' => FALSE,
      '#description' => '300 recommended',
    ];

    $form['search']['index_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Index Machine Name.'),
      '#default_value' => $config->get('index_name'),
      '#required' => FALSE,
      '#description' => 'The machine name of the search API index, ex: "site_index"',
    ];


    $form['prefixes'] = [
      '#type' => 'fieldset',
      '#title' => t('SirsiDynix Data Prefixes'),
      '#description' => 'Enter up to 6 strings that prefix the unique ID\'s used on your system',
      '#open' => TRUE,
    ];

    $form['prefixes']['prefix_one'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Prefix 1.'),
      '#default_value' => $config->get('prefix_one'),
      '#required' => FALSE,
      '#description' => 'The ID string before the unique identifier, ex: "ent://SD_ILS/0/SD_ILS:',
    ];

    $form['prefixes']['prefix_two'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Prefix 2.'),
      '#default_value' => $config->get('prefix_two'),
      '#required' => FALSE,
      '#description' => 'The ID string before the unique identifier, ex: "ent://SD_ILS/0/SD_ILS:',
    ];

    $form['prefixes']['prefix_three'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Prefix 3.'),
      '#default_value' => $config->get('prefix_three'),
      '#required' => FALSE,
      '#description' => 'The ID string before the unique identifier, ex: "ent://SD_ILS/0/SD_ILS:',
    ];

    $form['prefixes']['prefix_four'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Prefix 4.'),
      '#default_value' => $config->get('prefix_four'),
      '#required' => FALSE,
      '#description' => 'The ID string before the unique identifier, ex: "ent://SD_ILS/0/SD_ILS:',
    ];

    $form['prefixes']['prefix_five'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Prefix 5.'),
      '#default_value' => $config->get('prefix_five'),
      '#required' => FALSE,
      '#description' => 'The ID string before the unique identifier, ex: "ent://SD_ILS/0/SD_ILS:',
    ];

    $form['prefixes']['prefix_six'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Prefix 6.'),
      '#default_value' => $config->get('prefix_six'),
      '#required' => FALSE,
      '#description' => 'The ID string before the unique identifier, ex: "ent://SD_ILS/0/SD_ILS:',
    ];

    $form['actions']['#type'] = 'actions';

    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Save',
      '#button_type' => 'primary',
    );
    return parent::buildForm($form, $form_state);
	}

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * has to be here
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    #
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   *
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::SIRSI_SEARCH_SETTINGS)
      ->set('discovery_url', $form_state->getValue('discovery_url'))
      ->set('discovery_profile', $form_state->getValue('discovery_profile'))
      ->set('discovery_limit', $form_state->getValue('discovery_limit'))
      ->set('index_name', $form_state->getValue('index_name'))
      ->set('prefix_one', $form_state->getValue('prefix_one'))
      ->set('prefix_two', $form_state->getValue('prefix_two'))
      ->set('prefix_three', $form_state->getValue('prefix_three'))
      ->set('prefix_four', $form_state->getValue('prefix_four'))
      ->set('prefix_five', $form_state->getValue('prefix_five'))
      ->set('prefix_six', $form_state->getValue('prefix_six'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
