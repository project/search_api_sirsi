<?php

namespace Drupal\search_api_sirsi\EventSubscriber;

use Drupal;
use Drupal\search_api_sirsi\Item\SirsiItem;
use Drupal\search_api\Event\SearchApiEvents;
use Drupal\search_api\Event\ProcessingResultsEvent;
use Drupal\search_api\SearchApiException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;


/**
 * Wraps a processing results event.
 */
class SearchApiSirsiEventSubscriber implements EventSubscriberInterface {

  /**
   * @return array
   */
  public static function getSubscribedEvents() {
    $events[SearchApiEvents::PROCESSING_RESULTS][] = array('addMore');
    return $events;
  }

  /**
   * @param ProcessingResultsEvent $event
   * @throws SearchApiException
   *
   */
  public function addMore(ProcessingResultsEvent $event) {
    $connector = Drupal::service('sirsi_discovery.connector');
    $query = $event->getResults()->getQuery();
    $keys = $query->getKeys();
    $results = $event->getResults();
    $currentIndex = $event->getResults()->getQuery()->getIndex();
    $discoveryResults = $connector->searchDiscoverySearch($keys);
    $discoveryResultCnt = count($discoveryResults['entry']);
    $dataSource = $query->getIndex()->getDatasource('sirsi_data');
    foreach($discoveryResults['entry'] as $discoveryResult) {
      $exploded = explode(':', $discoveryResult['id']);
      $subExploded = explode('/',$exploded[1]);
      $startString = end($subExploded);
      $endString = end($exploded);
      $docId = 'sirsi_data/'.$startString.':'.$endString;
      $newResult = new SirsiItem($currentIndex, $docId , $dataSource);
      $scoreRaw = $discoveryResult['rel:score'];
      $convertedScore = $scoreRaw / $discoveryResultCnt;
      $finalScore = bcdiv($convertedScore, 100, 3);
      if($finalScore == 0) {
        $finalScore = 1.00;
      }
      $newResult->setScore($finalScore);
      $results->addResultItem($newResult);
      $resultCount = $results->getResultCount();
      $results->setResultCount($resultCount + 1);
    }
  }

}
