<?php
/**
 * @file
 * Contains \Drupal\search_api_sirsi\Service\DiscoverySearchService.
 */
namespace Drupal\search_api_sirsi\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

/**
 * Class DiscoverySearchService
 * @package Drupal\search_api_sirsi
 *
 */
class DiscoverySearchService {

  /**
   * @var Client
   */
  private Client $httpClient;

  /**
   * @var string
   */
  private $profileName;

  /**
   * @var string
   */
  private $resultLimit;

  /**
   * @var string
   */
  private $discoveryUrl;

  /**
   * @var XmlEncoder
   */
  private XmlEncoder $xmlEncoder;

  /**
   * DiscoverySearchService constructor.
   */
  public function __construct() {
    $localConfig = \Drupal::config('search_api_sirsi.adminsettings');
    $this->httpClient = \Drupal::httpClient();
    $this->xmlEncoder = new XmlEncoder();
    $this->discoveryUrl = $localConfig->get('discovery_url');
    $this->profileName = $localConfig->get('discovery_profile');
    $this->resultLimit = $localConfig->get('discovery_limit');
  }
  
  /**
   * @param $url
   * @param $params
   * @param $headers
   * @return false|mixed
   * GET request to Symphony Discovery Service.
   */
  protected function getFromDiscoverySearch($url, $params, $headers) {
    $data = FALSE;
    try {
      $response = $this->httpClient->get($url, [
        'headers' => $headers,
        'query' => $params,
      ]);
      $status = $response->getStatusCode();
      if($status == '200') {
        $data = $response->getBody()->getContents();
      } else {
        \Drupal::logger('search_api_sirsi')
          ->warning('getFromDiscoverySearch() returned a status '.$status. ' with the response '.$response->getBody()
              ->getContents());
      }
    } catch (RequestException $e) {
      watchdog_exception('search_api_sirsi - getFromDiscoverySearch()', $e);
    }
    return $data;
  }

  /**
   * @param null $query
   * @return array|false
   */
  public function searchDiscoverySearch($query = NULL) {
    $return = FALSE;
    $headers = $this->createGetHeaders();
    $params = [
      'pr' => $this->profileName,
      'ct' => $this->resultLimit,
      'q' => $query
    ];
    $data = $this->getFromDiscoverySearch($this->discoveryUrl, $params, $headers);
    if ($data) {
      $return = $this->xmlEncoder->decode($data, 'text/xml', []);
    }
    return $return;
  }

  /**
   * @param null $id
   * @return array|false
   *
   */
  public function getItemById($id = NULL) {
    $return = FALSE;
    $headers = $this->createGetHeaders();
    $sirsiId = $this->getLongFromId($id);
    $params = [
      'pr' => $this->profileName,
      'ext' => 'dss',
      'd' => $sirsiId,
    ];
    $data = $this->getFromDiscoverySearch($this->discoveryUrl, $params, $headers);
    if ($data) {
      $return = $this->xmlEncoder->decode($data, 'text/xml', []);
    }
    return $this->resultFilter($return);
  }

  /**
   * @return array
   * makes headers for GET requests
   */
  protected function createGetHeaders() {
    $headers = [
      'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
      'Accept-Encoding' => 'gzip, deflate, br',
    ];
    return $headers;
  }

  /**
   * @param null $result
   * @return array
   * Creates a more compact result array
   */
  public function resultFilter($result = NULL) {
    $resultArray = [];
    $finalArray = [];
    foreach($result as $item => $value) {
      switch($item) {
        case $item == 'entry':
          $resultArray['entry'] = $value;
          break;
        case $item == 'dss:child':
          $resultArray['child'] = $value;
          break;
      }
    }
    $id = $this->getIdFromLong($resultArray['entry']['id']);
    $finalArray[$id] = [
      'title' => $resultArray['entry']['title'],
      'summary' => $resultArray['entry']['summary'],
    ];
    foreach($resultArray['entry']['dss:field'] as $fieldArray) {
      $finalArray[$id][strtolower($fieldArray['@name'])] = $fieldArray['#'];
    }
    unset($finalArray[$id]['TITLE']);
    return $finalArray;
  }

  /**
   * @param null $id
   * @return string|FALSE
   * ent://ERC_222_3048/0/222_3048:OVERDRIVE:4434d4a6-b10e-48d5-a06b-43a0ca9a8b9b
   * ent://SD_ILS/0/SD_ILS:594327
   */
  public function getLongFromId($id = NULL) {
    $return = FALSE;
    $config = \Drupal::config('search_api_sirsi.adminsettings');
    $idArray = explode(':', $id);
    $countArray = [
      'prefix_one',
      'prefix_two',
      'prefix_three',
      'prefix_four',
      'prefix_five',
      'prefix_six'
    ];
    foreach($countArray as $elementId) {
      if($existingString = $config->get($elementId)) {
        if(str_contains($existingString, $idArray[0])) {
          $return = $existingString.$idArray[1];
        }
      }
    }
    return $return;
  }

  /**
   * @param null $id_string
   * @return mixed|string
   *
   */
  public function getIdFromLong($id_string = NULL) {
    $exploded = explode(':', $id_string);
    $subExploded = explode('/',$exploded[1]);
    $startString = end($subExploded);
    $endString = end($exploded);
    return $startString.':'.$endString;
  }

}

