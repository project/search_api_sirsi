<?php
/**
 * @file
 * Contains \Drupal\search_api_sirsi\Controller\pageController.
 */
namespace Drupal\search_api_sirsi\Controller;

use Drupal;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Pager\PagerManagerInterface;
use Drupal\search_api\Entity\Index;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\ParseMode\ParseModePluginManager;
use Drupal\search_api\Query\ResultSetInterface;
use Drupal\search_api\SearchApiException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;


class pageController extends ControllerBase {

  /**
   * The parse mode plugin manager.
   *
   * @var \Drupal\search_api\ParseMode\ParseModePluginManager
   */
  protected $parseModePluginManager;

  /**
   * The parse mode pager manager.
   *
   * @var Drupal\Core\Pager\PagerManagerInterface
   */
  protected PagerManagerInterface $pagerManager;

  /**
   * @var
   * The discovery search controller service
   */
  protected $discoveryController;

  /**
   * SearchApiPageController constructor.
   *
   * @param Drupal\Core\Pager\PagerManagerInterface $pagerManager
   *   The parse mode pager manager.
   */
  public function __construct(ParseModePluginManager $parseModePluginManager, PagerManagerInterface $pagerManager, $discoveryController) {
    $this->pagerManager = $pagerManager;
    $this->parseModePluginManager = $parseModePluginManager;
    $this->discoveryController = $discoveryController;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.search_api.parse_mode'),
      $container->get('pager.manager'),
      $container->get('sirsi_discovery.connector'),
    );
  }

  /**
   * @return string[]
   * makes a page
   */
  public function searchpage(Request $request) {
    $keys = $request->get('keys');
    $page = $request->get('page');
    $build['#theme'] = 'search_api_sirsi_search';
    $query = $this->prepareQuery($request);
    $query->keys($keys);
    ($page == 0) ? $pageStart = 0 : $pageStart = $page * 10;
    $results = [];
    $result = $query->execute();
    /* @var $items \Drupal\search_api\Item\ItemInterface[] */
    $items = $result->getResultItems();
    /* re-sort the combined results by score */
    $sortArray = [];
    $onesArray = [];
    $counter = 'a';
    foreach ($items as $item) {
      $score = (string) $item->getScore();
      ($score == 1) ? $onesArray[$counter++] = $item : $sortArray[$score] = $item;
    }
    krsort($sortArray);
    $sortArray = array_merge($sortArray, $onesArray);
    foreach ($sortArray as $item) {
      $finalRenderable[] = $item;
    }
    $final = array_slice($finalRenderable, $pageStart, 10);
    foreach ($final as $item) {
      $rendered = $this->createItemRenderArray($item);
      if ($rendered === []) {
        continue;
      }
      $results[] = $rendered;
    }
    if (empty($results)) {
      return $this->finishBuildWithoutResults($build);
    }
    return $this->finishBuildWithResults($build, $result, $results);
  }

  /**
   * @return string[]
   *
   */
  public function itempage($itemId = NULL) {
    $return = $this->discoveryController->getItemById($itemId);
    $var = '';
    if($return) {
      $var .= '<div id="sirsi-item-container">';
      if($return[$itemId]['initial_title_srch'] != NULL || $return[$itemId]['initial_title_srch'] != '') {
        $var .= '<h2 class="node__title">';
        $var .= $return[$itemId]['initial_title_srch'];
        $var .= '</h2>';
      }
      if($return[$itemId]['initial_author_srch'] != NULL || $return[$itemId]['initial_author_srch'] != '') {
        $var .= '<div class="c-feature">';
        $var .= '<div class="c-title">Author:</div> ' . $return[$itemId]['initial_author_srch'];
        $var .= '</div>';
      }
      if($return[$itemId]['library'] != NULL || $return[$itemId]['library'] != '') {
        $var .= '<div class="c-feature">';
        $var .= '<div class="c-title">Location:</div> '. $return[$itemId]['library'];
        $var .= '</div>';
      }
      if($return[$itemId]['bibsummary'] != NULL || $return[$itemId]['bibsummary'] != '') {
        $var .= '<div class="c-feature">';
        $var .= '<div class="c-title">Summary:</div> ' . $return[$itemId]['bibsummary'];
        $var .= '</div>';
      }
      if($return[$itemId]['pubdate'] != NULL || $return[$itemId]['pubdate'] != '') {
        $var .= '<div class="c-feature">';
        $var .= '<div class="c-title">Publish Date:</div> ' . $return[$itemId]['pubdate'];
        $var .= '</div>';
      }
      if($return[$itemId]['publisher'] != NULL || $return[$itemId]['publisher'] != '') {
        $var .= '<div class="c-feature">';
        $var .= '<div class="c-title">Publisher:</div> ' . $return[$itemId]['publisher'];
        $var .= '</div>';
      }
      $var .= '</div>'; /* END sirsi-item-container */
    } else {
      $var .= '<div id="sirsi-item-container">';
      $var .='<h2 class="node__title">';
      $var .= 'This resource is unavailable.';
      $var .= '</h2>';
      $var .= '<div class="c-feature">';
      $var .= '<div class="c-title">Sorry, we are having trouble finding that.</div>';
      $var .= '</div>';
      $var .= '</div>'; /* END sirsi-item-container */
    }
    $element = [
      '#type' => 'markup',
      '#markup' => $var,
    ];
    return $element;
  }

  /**
   * Prepares the search query.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Drupal\search_api\Query\QueryInterface
   *   The prepared query.
   *
   */
  protected function prepareQuery(Request $request) {
    $indexName = Drupal::config('search_api_sirsi.adminsettings')->get('index_name');
    /* @var $search_api_index \Drupal\search_api\IndexInterface */
    $index = Index::load($indexName);
    $query = $index->query([
      'limit' => 10,
      'offset' => $request->get('page') !== NULL ? $request->get('page') * 10 : 0,
    ]);
    /* parse mode */
    $parse_mode = Drupal::service('plugin.manager.search_api.parse_mode')
      ->createInstance('terms');
    $query->setParseMode($parse_mode);
    return $query;
  }

  /**
   * Creates a render array for the given result item.
   *
   * @param \Drupal\search_api\Item\ItemInterface $item
   *   The item to render.
   *
   * @return array
   *   A render array for the given result item.
   */
  protected function createItemRenderArray(ItemInterface $item) {

    try {
      $originalObject = $item->getOriginalObject();
      if ($originalObject === NULL) {
        return [];
      }
      /** @var \Drupal\Core\Entity\EntityInterface $entity */
      $entity = $originalObject->getValue();
    }
    catch (SearchApiException $e) {
      return [];
    }
    if (!$entity) {
      return [];
    }
    $viewedResult = [
      '#theme' => 'search_api_sirsi_search_result',
      '#item' => $item,
      '#entity' => $entity,
    ];
    return $viewedResult;
  }

  /**
   * Adds the no results text and then finishes the build.
   *
   * @param array $build
   *   The build to finish.
   * @param \Drupal\search_api_page\SearchApiPageInterface $searchApiPage
   *   The Search API page entity.
   *
   * @return array
   *   The finished build render array.
   */
  protected function finishBuildWithoutResults(array $build) {
    $build['#no_results_found'] = [
      '#markup' => 'Your search yielded no results.',
    ];
    $build['#search_help'] = [
      '#markup' => '<ul>
<li>Check if your spelling is correct.</li>
<li>Remove quotes around phrases to search for each word individually. <em>bike shed</em> will often show more results than <em>&quot;bike shed&quot;</em>.</li>
<li>Consider loosening your query with <em>OR</em>. <em>bike OR shed</em> will often show more results than <em>bike shed</em>.</li>
</ul>',
    ];
    return $build;
  }

  /**
   * Adds the results to the given build and then finishes it.
   *
   * @param array $build
   *   The build.
   * @param \Drupal\search_api\Query\ResultSetInterface $result
   *   Search API result.
   * @param array $results
   *   The result item render arrays.
   *
   * @return array
   *   The finished build.
   */
  protected function finishBuildWithResults(array $build, ResultSetInterface $result, array $results) {
    $build['#search_title'] = [
      '#markup' => 'Search results:',
    ];
    $resultCt = $result->getResultCount();
    ($resultCt >= 300) ? $slug = 'More than 300 results found.' : $slug = $resultCt.' results found';
    $build['#no_of_results'] = [
      '#markup' => $resultCt.' results found.',
    ];
    $build['#results'] = $results;
    $this->pagerManager->createPager($result->getResultCount(), 10);
    $build['#pager'] = [
      '#type' => 'pager',
    ];
    return $build;
  }


}
