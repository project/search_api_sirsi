<?php

namespace Drupal\search_api_sirsi\Plugin\search_api\datasource;


use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\Plugin\DataType\EntityAdapter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\search_api_sirsi\Entity\Sirsi;
use Drupal\search_api_sirsi\Service\DiscoverySearchService;
use Drupal\search_api\Datasource\DatasourcePluginBase;
use Drupal\search_api\IndexInterface;
use Drupal\search_api\Plugin\PluginFormTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal;

/**
 * Represents a datasource which connects to SirsiDynix API.
 *
 * @SearchApiDatasource(
 *   id = "sirsi_data",
 *   label = @Translation("Sirsi API Data"),
 *   description = @Translation("SirsiDynix Discovery Search based data."),
 * )
 */
class SirsiDataType extends DatasourcePluginBase implements PluginFormInterface {

  use PluginFormTrait;

  /**
   * @var
   */
  protected $index;

  /**
   * @var
   */
  protected $discoveryConnector;

  /**
   * The entity type bundle info.
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface|null
   */
  protected $entityTypeBundleInfo;

  /**
   * SirsiDataType constructor.
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   *
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    if (($configuration['#index'] ?? NULL) instanceof IndexInterface) {
      $this->setIndex($configuration['#index']);
      unset($configuration['#index']);
    }
    $this->pluginDefinition = $plugin_definition;
    $this->pluginId = $plugin_id;
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  /**
   * @param ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @return Drupal\Core\Plugin\ContainerFactoryPluginInterface|Drupal\search_api\Plugin\ConfigurablePluginBase|SirsiDataType|static
   *
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var static $datasource */
    $datasource = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $datasource->setDiscoveryConnector($container->get('sirsi_discovery.connector'));
    $datasource->setEntityTypeBundleInfo($container->get('entity_type.bundle.info'));
    return $datasource;
  }

  /**
   * @param ComplexDataInterface $item
   * @return EntityInterface|null
   *
   */
  protected function getEntity(ComplexDataInterface $item) {
    $value = $item->getValue();
    return $value instanceof EntityInterface ? $value : NULL;
  }

  /**
   * @param DiscoverySearchService $discovery_connector
   * @return $this
   *
   */
  public function setDiscoveryConnector(DiscoverySearchService $discovery_connector) {
    $this->discoveryConnector = $discovery_connector;
    return $this;
  }

  /**
   * @param null $id
   * @return string
   */
  public function getItemLanguage($id = NULL) {
    return 'en';
  }

  /**
   * @return mixed|string|null
   */
  public function getEntityTypeId() {
    $plugin_definition = $this->getPluginDefinition();
    ($plugin_definition) ? $typeId = 'sirsi' : $typeId = $plugin_definition['entity_type'];
    return $typeId;
  }

  /**
   * @return IndexInterface
   *
   */
  public function getIndex() {
    return $this->index;
  }

  /**
   * @param ComplexDataInterface $item
   * @return string|null
   *
   */
  public function getItemId(ComplexDataInterface $item) {
    if ($entity = $this->getEntity($item)) {
      $enabled_bundles = $this->getBundles();
      if (isset($enabled_bundles[$entity->bundle()])) {
        return $entity->id() . ':' . $entity->language()->getId();
      }
    }
    return NULL;
  }

  /**
   * @return array
   */
  public function getFieldsDefinition() {
    return array();
  }

  /**
   * @return array
   */
  public function getPropertyDefinitions() {
    $fields = $this->getFieldsDefinition();
    $properties = array();
    foreach ($fields as $field_id => $field_definition) {
      $properties[$field_id] = new DataDefinition($field_definition);
    }
    return $properties;
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   *
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = [];
    $form['markup'] = [
      '#markup' => 'There is no configuration for this data type.'
    ];
    return $form;
  }

  /**
   * @param mixed $id
   * @return EntityAdapter
   * @throws Drupal\Core\TypedData\Exception\ReadOnlyException
   *
   */
  public function load($id) {
    $bookData = $this->discoveryConnector->getItemById($id);
    $entity = new Sirsi($bookData);
    $dataDefinition = new DataDefinition();
    $dataDefinition->setLabel($id);
    $data = new EntityAdapter($dataDefinition);
    $data->setValue($entity);
    return $data;
  }

  /**
   * @param array $ids
   * @return array
   * @throws Drupal\Core\TypedData\Exception\ReadOnlyException
   *
   */
  public function loadMultiple(array $ids) {
    $data = [];
    if (!empty($ids) && is_array($ids)) {
      foreach ($ids as $id) {
        $data[$id] = $this->load($id);
      }
    }
    return $data;
  }

  /**
   * @return array
   *
   */
  public function defaultConfiguration() {
    $default_configuration = [];
    $default_configuration['bundles'] = [
      'default' => FALSE,
      'selected' => [],
    ];
    $default_configuration['languages'] = [
      'default' => FALSE,
      'selected' => [],
    ];
    return $default_configuration;
  }

  /**
   * @return array
   *
   */
  public function getBundles() {
    if (!$this->hasBundles()) {
      // For entity types that have no bundle, return a default pseudo-bundle.
      return [$this->getEntityTypeId() => $this->label()];
    }
    $configuration = $this->getConfiguration();
    // If "default" is TRUE (that is, "All except those selected"),remove all
    // the selected bundles from the available ones to compute the indexed
    // bundles. Otherwise, return all the selected bundles.
    $bundles = [];
    $entity_bundles = $this->getEntityBundles();
    $selected_bundles = array_flip($configuration['bundles']['selected']);
    $function = $configuration['bundles']['default'] ? 'array_diff_key' : 'array_intersect_key';
    $entity_bundles = $function($entity_bundles, $selected_bundles);
    foreach ($entity_bundles as $bundle_id => $bundle_info) {
      $bundles[$bundle_id] = $bundle_info['label'] ?? $bundle_id;
    }
    return $bundles ?: [$this->getEntityTypeId() => $this->label()];
  }

  /**
   * Determines whether the entity type supports bundles.
   *
   * @return bool
   *   TRUE if the entity type supports bundles, FALSE otherwise.
   */
  protected function hasBundles() {
    #return $this->getEntityType()->hasKey('bundle');
    return FALSE;
  }

  /**
   * Retrieves all bundles of this datasource's entity type.
   *
   * @return array
   *   An associative array of bundle infos, keyed by the bundle names.
   */
  protected function getEntityBundles() {
    return $this->hasBundles() ? $this->getEntityTypeBundleInfo()->getBundleInfo($this->getEntityTypeId()) : [];
  }

  /**
   * Retrieves the entity display repository.
   *
   * @return \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   *   The entity entity display repository.
   */
  public function getEntityTypeBundleInfo() {
    return $this->entityTypeBundleInfo ?: Drupal::service('entity_type.bundle.info');
  }

  /**
   * Sets the entity type bundle info.
   *
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The new entity type bundle info.
   *
   * @return $this
   */
  public function setEntityTypeBundleInfo(EntityTypeBundleInfoInterface $entity_type_bundle_info) {
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    return $this;
  }

}

